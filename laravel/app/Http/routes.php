<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('contato', 'HomeController@contatoPost')->name('contato.post');
    Route::post('agendamento', 'HomeController@agendamentoPost')->name('agendamento.post');
    Route::get('mensagens', 'MensagensController@index')->name('mensagens');
    Route::get('oracoes', 'OracoesController@index')->name('oracoes');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload'); 
        Route::post('order', 'PainelController@order'); 
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::get('calendario', 'CalendarioController@index')->name('calendario.index');
        Route::get('calendario/{dia}/{mes}/{ano}', 'CalendarioController@show')->name('calendario.show');
        Route::post('calendario/save', 'CalendarioController@store')->name('calendario.store');
        Route::post('calendario/delete', 'CalendarioController@destroy')->name('calendario.destroy');
        Route::get('agendamentos/confirmados', 'AgendamentosController@listarConfirmados')->name('agendamentos.confirmados');
        Route::resource('agendamentos', 'AgendamentosController');
        Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);
        Route::resource('oracoes', 'OracoesController');
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');
        
        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});