<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Mensagens;

use App\Http\Requests;



class MensagensController extends Controller
{
    public function index()
    {
        $response = file_get_contents('https://www.instagram.com/crisdosanjosoficial/?__a=1');
        $user = json_decode($response);
        $posts = $user->graphql->user->edge_owner_to_timeline_media->edges;

        return view('frontend.mensagens', compact('posts', 'user'));
    }
}
