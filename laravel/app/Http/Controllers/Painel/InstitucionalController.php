<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Institucional;
use App\Http\Requests\InstitucionalRequest;

class InstitucionalController extends Controller
{
    public function index()
    {
        $registro = Institucional::first();

        return view('painel.institucional.edit', compact('registro'));
    }

    public function update(InstitucionalRequest $request, Institucional $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_banner'])) $input['imagem_banner'] = Institucional::upload_imagem_banner();

            if (isset($input['imagem_frase_banner'])) $input['imagem_frase_banner'] = Institucional::upload_imagem_frase_banner();

            if (isset($input['imagem_perfil'])) $input['imagem_perfil'] = Institucional::upload_imagem_perfil();

            if (isset($input['imagem_anjos'])) $input['imagem_anjos'] = Institucional::upload_imagem_anjos();

            $registro->update($input);

            return redirect()->route('painel.institucional.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
