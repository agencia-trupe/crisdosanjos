<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Calendario;
use App\Helpers\Tools;
use Carbon\Carbon;

class CalendarioController extends Controller
{
    public function index()
    {
        if (empty($_GET["data"])) {
            $dados = $this->getCalendarioDia(date("d/m/Y"));
        } else {
            $dados = $this->getCalendarioDia($_GET["data"]);
        }

        return view('painel.calendario.index', compact('dados'));
    }

    public function show($dia, $mes, $ano)
    {
        $data = $dia . "/" . $mes . "/" . $ano;

        $dados = $this->getCalendarioDia($data);

        return response()->json($dados);
    }

    public function store(Request $request, Calendario $registro)
    {
        try {
            $data = $request['data'];
            $horario = $request['horario'];

            $dataFormat = \Carbon\Carbon::createFromFormat('d/m/Y', $data)->format('Y-m-d');

            $findId = Calendario::where('data', $dataFormat)->where('horario', $horario)->value('id');

            if (isset($findId)) {
                $dados = ['liberado' => 1];
                $registro->where('id', $findId)->update($dados);
            } else {
                $dados = [
                    'data' => $dataFormat,
                    'horario' => $horario,
                    'liberado' => 1
                ];
                $registro->create($dados);
            }

            return redirect()->route('calendario.index', compact('data'))->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Request $request, Calendario $registro)
    {
        try {
            $data = $request['data'];
            $horario = $request['horario'];

            $dataFormat = \Carbon\Carbon::createFromFormat('d/m/Y', $data)->format('Y-m-d');

            $findId = Calendario::where('data', $dataFormat)->where('horario', $horario)->value('id');

            $registro->where('id', $findId)->delete();

            return redirect()->route('calendario.index', compact('data'))->with('success', 'Registro removido com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao remover registro: ' . $e->getMessage()]);
        }
    }

    private function getCalendarioDia($data)
    {
        $dataFormat = \Carbon\Carbon::createFromFormat('d/m/Y', $data)->format('Y-m-d');
        $registros = Calendario::where('data', $dataFormat)->get();

        $inicio = new \DateTime('07:30');
        $intervalo = 14.5 * 2; // 08:00 as 22:00 -> total 29 horarios

        for ($i = 0; $i < $intervalo; $i++) {

            $horarios[] = $inicio->add(new \DateInterval('PT30M'))->format('H:i');
            $registroHorario = $registros->where('horario', $horarios[$i]);

            if ($registroHorario->isEmpty()) { // se vazio
                $dados[] =  [$horarios[$i], 0];
            } else {
                foreach ($registroHorario as $registro) {

                    if (empty($registro)) {
                        $dados[] =  [$horarios[$i], 0];
                    } else {
                        if ($registro->liberado == 1) {
                            $dados[] =  [$horarios[$i], 1];
                        } else {
                            $dados[] =  [$horarios[$i], 0];
                        }
                    }
                }
            }
        }

        return $dados;
    }
}
