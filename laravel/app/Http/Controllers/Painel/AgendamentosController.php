<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\AgendamentosRequest;
use App\Models\Agendamento;
use App\Models\Calendario;
use App\Models\TipoAtendimento;
use Illuminate\Support\Facades\Mail;

class AgendamentosController extends Controller
{
    public function index()
    {
        $agendamentos = Agendamento::orderBy('created_at', 'DESC')->get();

        return view('painel.agendamentos.index', compact('agendamentos'));
    }

    public function show($id)
    {
        $pedido = $id;

        $calendario = Calendario::find($pedido->calendario_id);

        $tipoAtendimento = TipoAtendimento::find($pedido->tipo_atendimento_id);

        return view('painel.agendamentos.show', compact('pedido', 'calendario', 'tipoAtendimento'));
    }

    public function update(Agendamento $agendamento, Request $request)
    {
        try {

            $agendamento->update([
                'confirmado' => !$agendamento->confirmado
            ]);

            $this->sendMail($agendamento);

            return redirect()->route('painel.agendamentos.index')->with('success', 'Pedido de agendamento confirmado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar pedido de agendamento: ' . $e->getMessage()]);
        }
    }

    public function destroy(Agendamento $agendamento)
    {
        try {

            $agendamento->delete();
            return redirect()->route('painel.agendamentos.index')->with('success', 'Pedido de agendamento excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir pedido: ' . $e->getMessage()]);
        }
    }

    public function listarConfirmados()
    {
        $agendamentosConfirmados = Agendamento::where('confirmado', 1)->orderBy('created_at', 'DESC')->get();

        return view('painel.agendamentos.confirmados', compact('agendamentosConfirmados'));
    }

    private function sendMail(Agendamento $agendamento)
    {
        $calendario = Calendario::find($agendamento->calendario_id);
        $tipoAtendimento = TipoAtendimento::find($agendamento->tipo_atendimento_id);

        $dadosEmail = array(
            'data' => $calendario->data,
            'horario' => $calendario->horario,
            'tipoAtendimento' => $tipoAtendimento->tipo,
            'nome' => $agendamento->nome,
        );

        if (isset($agendamento->email)) {
            Mail::send('emails.agendamento', $dadosEmail, function ($message) use ($agendamento) {
                $message->to($agendamento->email, $agendamento->nome)
                    ->subject('[AGENDAMENTO] Cris dos Anjos')
                    ->from('exemplo@email.com', '[AGENDAMENTO] Cris dos Anjos');
            });
        }
    }
}
