<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\OracoesRequest;
use App\Models\Oracoes;

class OracoesController extends Controller
{
    private $oracoes;

    public function __construct()
    {
        $this->oracoes = Oracoes::ordenados()->lists('titulo', 'id');
    }

    public function index()
    {
        $registros = Oracoes::ordenados()->get();

        return view('painel.oracoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.oracoes.create');
    }

    public function store(OracoesRequest $request)
    {
        try {

            $input = $request->all();

            Oracoes::create($input);

            return redirect()->route('painel.oracoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Oracoes $registro)
    {
        return view('painel.oracoes.edit', compact('registro'));
    }

    public function update(OracoesRequest $request, Oracoes $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.oracoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Oracoes $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.oracoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
