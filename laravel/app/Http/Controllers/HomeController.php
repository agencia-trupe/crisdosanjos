<?php

namespace App\Http\Controllers;

use App\Models\Calendario;
use App\Models\Institucional;
use App\Models\Agendamento;
use App\Models\TipoAtendimento;
use App\Models\ContatoRecebido;
use App\Models\Contato;

use App\Http\Requests\AgendamentosRequest;
use App\Http\Requests\ContatosRecebidosRequest;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $dados = Institucional::first();

        $dataHoje = date("Y-m-d");

        $calendarios = Calendario::orderBy('data', 'asc')->where('data', '>=', $dataHoje)->get();
        $calendariosData = $calendarios->groupBy('data');

        $diaSemana = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];

        $contato = Contato::first();

        return view('frontend.home', compact('dados', 'calendarios', 'calendariosData', 'contato', 'diaSemana'));
    }

    public function agendamentoPost(AgendamentosRequest $request, Agendamento $agendamento)
    {
        $input = $request->all();

        $calendarioId = Calendario::where('data', $input['data'])->where('horario', $input['horario'])->select('id')->get();
        foreach ($calendarioId as $id) {
            $input['calendario_id'] = $id->id;
        }

        $tipoAtendimentoId = TipoAtendimento::where('id', $input['tipo_atendimento'])->get();
        foreach ($tipoAtendimentoId as $id) {
            $input['tipo_atendimento_id'] = $id->id;
        }

        $agendamento->create($input);
        $this->sendMailAgendamento($input);

        return redirect('/')->with('enviado', true);
    }

    public function contatoPost(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMailContato($data);

        return redirect('/')->with('enviado', true);
    }

    private function sendMailContato($data) // CONTATO
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('emails.contato', $data, function ($m) use ($email, $data) {
            $m->to($email, config('app.name'))
                ->subject('[CONTATO] ' . config('app.name'))
                ->replyTo($data['email'], $data['nome']);
        });
    }

    private function sendMailAgendamento($input) // AGENDAMENTO
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('emails.pedido', $input, function ($m) use ($email, $input) {
            $m->to($email, config('app.name'))
                ->subject('[PEDIDO DE AGENDAMENTO] ' . config('app.name'))
                ->replyTo($input['email'], $input['nome']);
        });
    }
}
