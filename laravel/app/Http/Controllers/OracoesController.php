<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Oracoes;

class OracoesController extends Controller
{
    public function index() 
    {
        $registros = Oracoes::ordenados()->get();

        return view('frontend.oracoes', compact('registros'));
    }
}
