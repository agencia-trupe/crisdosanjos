<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitucionalRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_frase_banner'   => 'image',
            'imagem_banner'         => 'image',
            'titulo_banner'          => 'required',
            'frase_banner'          => 'required',

            'titulo_perfil'          => 'required',
            'frase_perfil'          => 'required',
            'texto_perfil'          => 'required',
            'imagem_perfil'         => 'image',

            'titulo_taro'            => 'required',
            'frase_taro'            => 'required',
            'texto_taro'            => 'required',

            'titulo_numerologia'     => 'required',
            'texto_numerologia'     => 'required',
            
            'imagem_anjos'           => 'image',

        ];
    }

    public function messages() {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'image'    => 'Inclua uma imagem corretamente',
        ];
    }
}
