<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgendamentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required'
        ];
    }

    public function messages() {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'email'    => 'Este campo deve conter um e-mail válido',
        ];
    }
}
