<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];

    public static function upload_imagem_banner()
    {
        return CropImage::make('imagem_banner', [
            'width'  => 1200,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_frase_banner()
    {
        return CropImage::make('imagem_frase_banner', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_perfil()
    {
        return CropImage::make('imagem_perfil', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_anjos()
    {
        return CropImage::make('imagem_anjos', [
            'width'  => 600,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }
}
