<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoAtendimento extends Model
{
    protected $table = 'tipo_atendimento';

    protected $guarded = ['id'];

    protected $fillable = [
        'tipo',
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function agendamentos()
    {
        return $this->hasMany('App\Models\Agendamento', 'tipo_atendimento_id')->ordenados();
    }
}
