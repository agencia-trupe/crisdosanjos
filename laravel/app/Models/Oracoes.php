<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Oracoes extends Model
{
    protected $table = 'oracoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
