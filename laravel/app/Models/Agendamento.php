<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendamento extends Model
{
    protected $table = 'agendamentos';

    protected $guarded = ['id'];

    protected $fillable = [
        'nome', 'email', 'telefone', 'confirmado', 'calendario_id', 'tipo_atendimento_id',
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function calendario()
    {
        return $this->belongsTo('App\Models\Calendario', 'calendario_id')->ordenados();
    }

    public function tipoAtendimento()
    {
        return $this->belongsTo('App\Models\TipoAtendimento', 'tipo_atendimento_id')->ordenados();
    }

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }

    public function scopeNaoConfirmados($query)
    {
        return $query->where('confirmado', '!=', 1);
    }

    public function countNaoConfirmados()
    {
        return $this->naoConfirmados()->count();
    }
}
