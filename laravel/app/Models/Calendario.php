<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{
    protected $table = 'calendario';

    protected $guarded = ['id'];

    protected $fillable = [
        'data', 'horario', 'liberado',
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeNaoLiberados($query)
    {
        return $query->where('liberado', '!=', 1);
    }

    public function agendamentos()
    {
        return $this->hasMany('App\Models\Agendamento', 'calendario_id')->ordenados();
    }
}
