<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome' => 'Cris dos Anjos',
            'email' => 'contato@trupe.net',
            'telefone' => '11 97865 4321',
            'instagram' => '@crisdosanjosoficial',
            'instagram_link' => 'https://www.instagram.com/crisdosanjosoficial/',
            'facebook' => 'facebook.com/tarocrisdosanjos.terapiaholistica',
            'facebook_link' => 'https://www.facebook.com/tarocrisdosanjos.terapiaholistica',
        ]);
    }
}
