<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoAtendimentoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipo_atendimento')->insert([
            'tipo'     => 'Presencial',
        ]);

        DB::table('tipo_atendimento')->insert([
            'tipo'     => 'Online',
        ]);
    }
}
