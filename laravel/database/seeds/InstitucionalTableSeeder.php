<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InstitucionalTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'imagem_frase_banner' => '',
            'titulo_banner' => 'Leitura de Tarô dos Anjos e Numerologia',
            'frase_banner'  => 'On-line ou presencial ∙ Agende a sua consulta agora!',
            'imagem_banner' => '',

            'titulo_perfil' => 'Terapeuta Holística',
            'frase_perfil' => 'Sou Terapeuta Holística e trabalho com o Tarô dos Anjos. Tenho o dom da visão e ajudo pessoas a encontrarem a felicidade e o seu verdadeiro Eu.',
            'texto_perfil' => '',
            'imagem_perfil' => '',

            'titulo_taro' => 'Leitura de Tarô dos Anjos',
            'frase_taro' => 'Aconselhamento para o equilíbrio pessoal em todas as áreas da vida.',
            'texto_taro' => '',

            'imagem_anjos' => '',

            'titulo_numerologia' => 'Numerologia',
            'texto_numerologia' => '',
        ]);
    }
}
