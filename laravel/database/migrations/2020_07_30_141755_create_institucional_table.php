<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateInstitucionalTable extends Migration
{
    public function up()
    {
        Schema::create('institucional', function (Blueprint $table) {
            $table->increments('id');

            $table->string('imagem_frase_banner');
            $table->string('titulo_banner');
            $table->string('frase_banner');
            $table->string('imagem_banner');

            $table->string('titulo_perfil');
            $table->string('frase_perfil');
            $table->text('texto_perfil');
            $table->string('imagem_perfil');

            $table->string('titulo_taro');
            $table->string('frase_taro');
            $table->text('texto_taro');

            $table->string('imagem_anjos');

            $table->text('titulo_numerologia');
            $table->text('texto_numerologia');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('institucional');
    }
}
