<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkTipoAtendimentoToAgendamentosTable extends Migration
{
    public function up()
    {
        Schema::table('agendamentos', function (Blueprint $table) {
            $table->integer('tipo_atendimento_id')->unsigned()->after('telefone');
            $table->foreign('tipo_atendimento_id')->references('id')->on('tipo_atendimento');
        });
    }

    public function down()
    {
        Schema::table('agendamentos', function (Blueprint $table) {
            $table->dropForeign(['tipo_atendimento_id']);
        });
    }
}
