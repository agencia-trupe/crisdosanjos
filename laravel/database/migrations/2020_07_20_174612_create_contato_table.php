<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('instagram');
            $table->string('instagram_link');
            $table->string('facebook');
            $table->string('facebook_link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contato');
    }
}
