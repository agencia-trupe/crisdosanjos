<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkCalendarioToAgendamentosTable extends Migration
{
    public function up()
    {
        Schema::table('agendamentos', function (Blueprint $table) {
            $table->integer('calendario_id')->unsigned()->after('id');
            $table->foreign('calendario_id')->references('id')->on('calendario')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('agendamentos', function (Blueprint $table) {
            $table->dropForeign(['calendario_id']);
        });
    }
}
