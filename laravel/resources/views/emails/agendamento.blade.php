<!DOCTYPE html>
<html>

<head>
    <title>[AGENDAMENTO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <p style='font-weight:bold;font-size:20px;font-family:Verdana;'>Olá, {{ $nome }}</p>
    <br>

    <p style='font-weight:500;font-size:16px;font-family:Verdana;'>O seu agendamento foi confirmado! Os dados do seu agendamento são:</p>

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Data:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Horário:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $horario }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo de atendimento:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $tipoAtendimento }}</span>

    <br>
    <br>
    <p style='font-weight:500;font-size:16px;font-family:Verdana;'>{{ config('app.name') }}</p>
    <a href="{{ route('home') }}" target="_blank" style='font-weight:400;font-size:16px;font-family:Verdana;'>www.crisdosanjos.com.br</a>
</body>

</html>