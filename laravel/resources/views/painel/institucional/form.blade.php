@include('painel.common.flash')

<h3>Banner:</h3>

<div class="form-group">
    {!! Form::label('titulo_banner', 'Título - Banner') !!}
    {!! Form::text('titulo_banner', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_banner', 'Frase - Banner') !!}
    {!! Form::text('frase_banner', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_frase_banner', 'Imagem - Frase Banner') !!}
    @if($registro->imagem_frase_banner)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem_frase_banner) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_frase_banner', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_banner', 'Imagem - Banner') !!}
    @if($registro->imagem_banner)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem_banner) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_banner', ['class' => 'form-control']) !!}
</div>

<hr>
<h3>Perfil:</h3>

<div class="form-group">
    {!! Form::label('titulo_perfil', 'Título - Perfil') !!}
    {!! Form::text('titulo_perfil', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_perfil', 'Frase - Perfil') !!}
    {!! Form::text('frase_perfil', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_perfil', 'Texto - Perfil') !!}
    {!! Form::textarea('texto_perfil', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_perfil', 'Imagem - Perfil') !!}
    @if($registro->imagem_perfil)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem_perfil) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_perfil', ['class' => 'form-control']) !!}
</div>

<hr>
<h3>Trabalho:</h3>

<div class="form-group">
    {!! Form::label('titulo_taro', 'Título - Tarô') !!}
    {!! Form::text('titulo_taro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_taro', 'Frase - Tarô') !!}
    {!! Form::text('frase_taro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_taro', 'Texto - Tarô') !!}
    {!! Form::textarea('texto_taro', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_numerologia', 'Título - Numerologia') !!}
    {!! Form::text('titulo_numerologia', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_numerologia', 'Texto - Numerologia') !!}
    {!! Form::textarea('texto_numerologia', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_anjos', 'Imagem - Tarô e Numerologia') !!}
    @if($registro->imagem_anjos)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem_anjos) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_anjos', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
