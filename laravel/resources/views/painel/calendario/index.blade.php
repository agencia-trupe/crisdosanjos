@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Calendário para Agendamentos
    </h2>
</legend>

<div class="alert alert-block" id="mensagem-calendario" style="display: none;">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    {!! session('success') !!}
</div>

<div class="calendario">
    <div class="datepicker calendario-mes"></div>
    <div class="calendario-dia">
        <table class="table table-striped table-bordered table-hover tabela-calendario">
            <h4 id="dataSelecionada"></h4>
            <thead>
                <tr>
                    <th>Horário</th>
                    <th>Status</th>
                    <th><span class="glyphicon glyphicon-cog"></span> Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($dados as $dado)
                <tr class="tr-row">
                    <td class="horario">{{ $dado[0] }}</td>
                    <td id="{!! str_replace(':', '', $dado[0]) !!}">
                        @if($dado[1] == 1)
                        Liberado para agendamento!
                        @else
                        Não liberado.
                        @endif
                    </td>
                    <td class="crud-actions">


                        <div class="btn-group btn-group-sm">
                            <form action="{{ route('calendario.store') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="data" class="input-data" value="">
                                <input type="hidden" name="horario" class="input-horario" value="{{ $dado[0] }}">
                                <input type="hidden" name="liberado" class="input-liberado" value="">
                                <button class="btn btn-primary btn-sm pull-left btn-add" type="submit" style="margin-right:5px;">
                                    <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar
                                </button>
                            </form>

                            <form action="{{ route('calendario.destroy') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="data" class="input-data" value="">
                                <input type="hidden" name="horario" class="input-horario" value="{{ $dado[0] }}">
                                <input type="hidden" name="liberado" class="input-liberado" value="">
                                <button type="submit" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Remover</button>
                            </form>
                            

                        </div>

                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection