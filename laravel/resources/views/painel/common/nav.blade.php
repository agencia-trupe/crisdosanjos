<ul class="nav navbar-nav">
  <li @if(Tools::routeIs('painel.institucional*')) class="active" @endif>
    <a href="{{ route('painel.institucional.index') }}">Institucional</a>
  </li>
  <li @if(Tools::routeIs('painel.oracoes*')) class="active" @endif>
    <a href="{{ route('painel.oracoes.index') }}">Orações</a>
  </li>
  <li @if(Tools::routeIs('painel.calendario*')) class="active" @endif>
    <a href="{{ route('calendario.index') }}">Calendário</a>
  </li>
  <li class="dropdown @if(Tools::routeIs('painel.agendamentos*')) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      Agendamentos
      @if($agendamentosNaoConfirmados >= 1)
      <span class="label label-success" style="margin-left:3px;">{{ $agendamentosNaoConfirmados }}</span>
      @endif
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.agendamentos*')) class="active" @endif>
        <a href="{{ route('painel.agendamentos.index') }}">
          Pedidos de agendamento
          @if($agendamentosNaoConfirmados >= 1)
          <span class="label label-success" style="margin-left:3px;">{{ $agendamentosNaoConfirmados }}</span>
          @endif
        </a>
      </li>
      <li @if(Tools::routeIs('agendamentos.confirmados')) class="active" @endif>
        <a href="{{ route('agendamentos.confirmados') }}">Agendamentos confirmados</a>
      </li>
    </ul>
  </li>
  <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      Contatos
      @if($contatosNaoLidos >= 1)
      <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
      @endif
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
      </li>
      <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
        <a href="{{ route('painel.contato.recebidos.index') }}">
          Contatos Recebidos
          @if($contatosNaoLidos >= 1)
          <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
          @endif
        </a>
      </li>
    </ul>
  </li>
</ul>