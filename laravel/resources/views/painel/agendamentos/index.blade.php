@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>Pedidos de agendamento recebidos</h2>
</legend>

@if(!count($agendamentos))
<div class="alert alert-warning" role="alert">Nenhum pedido de agendamento recebido.</div>
@else
<table class="table table-striped table-bordered table-hover data-table">
    <thead>
        <tr>
            <th>Data Pedido</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Telefone</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($agendamentos as $agendamento)

        <tr class="tr-row @if(!$agendamento->confirmado) warning @endif">
            <td data-order="{{ $agendamento->created_at_order }}">{{ $agendamento->created_at }}</td>
            <td>{{ $agendamento->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $agendamento->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $agendamento->email }}
            </td>
            <td>{{ $agendamento->telefone }}</td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">

                    <a href="{{ route('painel.agendamentos.show', $agendamento->id ) }}" class="btn btn-primary btn-sm pull-left" style="margin-right:5px;">
                        <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Detalhes
                    </a>

                    {!! Form::open([
                    'route' => ['painel.agendamentos.destroy', $agendamento->id],
                    'method' => 'delete'
                    ]) !!}
                    <button type="submit" class="btn btn-danger btn-sm btn-delete" style="margin-right:5px;"><span class="glyphicon glyphicon-remove" style="margin-right:10px;margin-left:10px;"></span></button>
                    {!! Form::close() !!}

                    {!! Form::open([
                    'route' => ['painel.agendamentos.update', $agendamento->id],
                    'method' => 'patch'
                    ]) !!}

                    @if(!$agendamento->confirmado)
                    <button type="submit" class="btn btn-sm pull-left btn-confirmar-agendamento btn-success"><span class="glyphicon small glyphicon-ok" style="margin-right:10px;margin-left:10px;"></span></button>
                    @endif
                    
                    {!! Form::close() !!}

                </div>
            </td>
        </tr>

        @endforeach
    </tbody>
</table>
@endif

@stop