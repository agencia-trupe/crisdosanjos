@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Pedido de agendamento</h2>
    </legend>

    <div class="form-group">
        <label>Data do pedido</label>
        <div class="well">{{ $pedido->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Tipo de atendimento</label>
        <div class="well">{{ $tipoAtendimento->tipo }}</div>
    </div>

    <div class="form-group">
        <label>Data de atendimento</label>
        <div class="well">{{ $calendario->data }}</div>
    </div>

    <div class="form-group">
        <label>Horário de atendimento</label>
        <div class="well">{{ $calendario->horario }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $pedido->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $pedido->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $pedido->email }}
        </div>
    </div>

@if($pedido->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $pedido->telefone }}</div>
    </div>
@endif

    

    <a href="{{ route('painel.agendamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
