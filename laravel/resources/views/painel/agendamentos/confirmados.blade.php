@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>Agendamentos Confirmados</h2>
</legend>

@if(!count($agendamentosConfirmados))
<div class="alert alert-warning" role="alert">Nenhum agendamento confirmado.</div>
@else
<table class="table table-striped table-bordered table-hover data-table">
    <thead>
        <tr>
            <th>Data</th>
            <th>Horário</th>
            <th>Tipo</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Telefone</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($agendamentosConfirmados as $agendamento)
        <tr class="tr-row @if(!$agendamento->confirmado) warning @endif">
            <td>{{ date('d/m/Y', strtotime(App\Models\Calendario::find($agendamento['calendario_id'])->data)) }}</td>
            <td>{{ App\Models\Calendario::find($agendamento['calendario_id'])->horario }}</td>
            <td>{{ App\Models\TipoAtendimento::find($agendamento['tipo_atendimento_id'])->tipo }}</td>
            <td>{{ $agendamento->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $agendamento->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $agendamento->email }}
            </td>
            <td>{{ $agendamento->telefone }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@stop