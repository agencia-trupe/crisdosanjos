@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Orações /</small> Editar Oração</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.oracoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.oracoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
