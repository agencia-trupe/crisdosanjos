@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Orações /</small> Adicionar Oração</h2>
    </legend>

    {!! Form::open(['route' => 'painel.oracoes.store', 'files' => true]) !!}

        @include('painel.oracoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
