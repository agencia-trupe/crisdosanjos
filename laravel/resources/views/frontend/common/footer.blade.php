<footer>
    <div class="footer">
        <p>© {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados</p>
    </div>
</footer>