<div class="itens-menu">
    <!-- HOME -->
    <a href="#homePerfil"><span class="glyphicon glyphicon-minus"></span>Terapeuta Holística Cris dos Anjos</a>
    <a href="#homeTrabalho"><span class="glyphicon glyphicon-minus"></span>Leitura de Tarô dos Anjos</a>
    <a href="#homeTrabalho"><span class="glyphicon glyphicon-minus"></span>Numerologia</a>
    <a href="#homeAgendamento"><span class="glyphicon glyphicon-minus"></span>Atendimento On-line ou Presencial</a>
    <a href="#homeAgendamento"><span class="glyphicon glyphicon-minus"></span>Agende seu atendimento</a>
    <a href="#homeContato"><span class="glyphicon glyphicon-minus"></span>Contato</a>

    <br>
    <!-- MENSAGENS -->
    <a href="{{ route('mensagens') }}" @if(Tools::routeIs('mensagens')) class="active" @endif><span class="glyphicon glyphicon-minus"></span>Mensagem dos anjos</a>
    <a href="{{ route('oracoes') }}" @if(Tools::routeIs('oracoes')) class="active" @endif><span class="glyphicon glyphicon-minus"></span>Orações</a>

</div>