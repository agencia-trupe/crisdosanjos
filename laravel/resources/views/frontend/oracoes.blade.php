@extends('frontend.common.template')

@section('content')

<div class="oracoes">
	<div class="center masonry-grid">
		@foreach($registros as $registro)
		<div class="oracao grid-item">
			<p>{{ $registro->titulo }}</p>
			<div>{!! $registro->texto !!}</div>
		</div>
		@endforeach
	</div>

</div>

@endsection