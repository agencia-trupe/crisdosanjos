<div class="dados-agendamento" id="homeAgendamento">
    <h2 class="titulo">Atendimento online ou presencial</h2>
    <p class="texto">As consultas podem ser realizadas via internet ou presencialmente, ambos formatos com agendamento prévio.</p>
    <p class="texto">Confira abaixo os próximos horários disponíveis, selecione o seu e aguarde confirmação via e-mail ou telefone.</p>

    <div class="tabelas-calendario">
        @foreach($calendariosData as $data => $values)
        <div class="tabela-dia">
            <h2 class="calendario-data">{{ strftime("%d %B", strtotime($data)) }}</h2>
            <hr>
            <p class="dia-semana">{{ $diaSemana[date('w', strtotime($data))] }}</p>
            <div class="horarios">
                @foreach($values->sortBy('horario') as $calendario)
                @php
                if($calendario->horario > '07:59' && $calendario->horario < '12:01' ) { $periodo='manhã' ; } elseif($calendario->horario > '12:00' && $calendario->horario < '18:01' ) { $periodo='tarde' ; } elseif($calendario->horario > '18:00' && $calendario->horario < '22:01' ){ $periodo='noite' ; } @endphp <button type="button" class="btn btn-primary btn-lg btn-modal-agendamento" data-toggle="modal" data-target="#modalAgendamento" value="{{ strftime('%d %B', strtotime($data)) }}" semana="{{ $diaSemana[date('w', strtotime($data))] }}" completa="{{ $data }}">
                            <p class="periodo">{{ $periodo }}: </p>
                            <p class="horario">{{ $calendario->horario }}</p>
                            </button>
                            @endforeach
            </div>
        </div>
        @endforeach

        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            Mensagem enviada com sucesso!
        </div>
        @endif
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalAgendamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="{{ asset('assets/img/layout/icone-fechar.png') }}" alt=""></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <h4 class="modal-texto" id="myModalLabel">Você selecionou o seguinte horário:</h4>
            <h2 class="modal-title">
                <p class="dia-semana"></p> - <p class="dia-data"></p> - <p class="dia-horario"></p>
            </h2>
            <form action="{{ route('agendamento.post') }}" method="POST">
                {!! csrf_field() !!}
                <h4 class="frase">Informe seus dados para receber a confirmação do agendamento:</h4>
                <input type="hidden" name="data" class="input-data" value="">
                <input type="hidden" name="horario" class="input-horario" value="">
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone" required>
                <div class="tipo-agendamento">
                    <input type="checkbox" id="checkbox1" class="presencial" name="tipo_atendimento" value="1">
                    <label for="checkbox1">atendimento presencial</label>
                    <input type="checkbox" id="checkbox2" class="on-line" name="tipo_atendimento" value="2">
                    <label for="checkbox2">atendimento on-line</label>
                </div>
                <button type="submit" class="btn btn-primary" id="btnAddAgendamento" data-toggle="modal" data-target="#modalAgendamentoMsg">Solicitar agendamento</button>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAgendamentoMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="{{ asset('assets/img/layout/icone-fechar.png') }}" alt=""></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content-msg">
            <h1 class="modal-title-msg" id="myModalLabel">GRATA!</h1>
            <h4 class="modal-texto-msg">Você receberá confirmação do seu horário por e-mail ou telefone em breve.</h4>
        </div>
    </div>
</div>