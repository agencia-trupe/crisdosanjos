@extends('frontend.common.template')

@section('content')

<div class="home">

    <div class="banner">
        <img src="{{ asset('assets/img/institucional/'.$dados->imagem_frase_banner) }}" alt="" class="frase-banner">
        <p class="titulo">{{ $dados->titulo_banner }}</p>
        <p class="texto">{{ $dados->frase_banner }}</p>
        <img src="{{ asset('assets/img/institucional/'.$dados->imagem_banner) }}" alt="" class="img-banner">
        <a class="instagram" href="{{ $contato->instagram_link }}" target="_blank">
            <p class="titulo">Me siga no instagram</p>
            <img src="{{ asset('assets/img/layout/icone-instagram.png') }}" alt="">
            <p class="instagram">{{ $contato->instagram }}</p>
        </a>
    </div>

    <div class="dados-perfil" id="homePerfil">
        <div class="foto-nome">
            <img src="{{ asset('assets/img/institucional/'.$dados->imagem_perfil) }}" alt="">
            <div class="titulo-perfil">
                <p>{{ $dados->titulo_perfil }}</p>
                <p>{{ $config->title }}</p>
            </div>
        </div>
        <div class="textos-perfil">
            <h2 class="frase">{{ $dados->frase_perfil }}</h2>
            <div class="texto">{!! $dados->texto_perfil !!}</div>
        </div>
    </div>

    <div class="dados-trabalho" id="homeTrabalho">
        <div class="taro">
            <div class="center">
                <h2 class="titulo">{{ $dados->titulo_taro }}</h2>
                <p class="frase">{{ $dados->frase_taro }}</p>
                <div class="texto">{!! $dados->texto_taro !!}</div>
            </div>
        </div>
        <div class="numerologia">
            <div class="center">
                <h2 class="titulo">{{ $dados->titulo_numerologia }}</h2>
                <div class="texto">{!! $dados->texto_numerologia !!}</div>
            </div>
        </div>
        <div class="center">
            <img src="{{ asset('assets/img/institucional/'.$dados->imagem_anjos) }}" alt="">
        </div>
    </div>

    @include('frontend.agendamento')

    @include('frontend.contato')

</div>

@endsection