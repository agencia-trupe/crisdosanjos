<div class="content contato" id="homeContato">
    <h2 class="titulo">Contato</h2>

    <div class="center">
        <div class="informacoes">
            <p>Utilize nosso agendamento para marcar sua consulta on-line ou presencial.</p>
            <p>Mas se precisar mesmo falar conosco, aí vai nosso número: <a href="https://api.whatsapp.com/send?phone={{ $contato->telefone }}" class="telefone" target="_blank">{{ $contato->telefone }}</a></p>
        </div>

        <form action="{{ route('contato.post') }}" method="POST">
            {!! csrf_field() !!}
            <div class="dados-contato">
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
            </div>

            <button type="submit" class="btn-contato">Enviar</button>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                Mensagem enviada com sucesso!
            </div>
            @endif
        </form>

        <div class="redes-sociais">
            <p>Siga nossas redes sociais:</p>
            <div class="instagram">
                <img src="{{ asset('assets/img/layout/icone-instagram.png') }}" alt="">
                <a href="{{ $contato->instagram_link }}" target="_blank">{{ $contato->instagram }}</a>
            </div>
            <div class="facebook">
                <img src="{{ asset('assets/img/layout/icone-facebook.png') }}" alt="">
                <a href="{{ $contato->facebook_link }}" target="_blank">{{ $contato->facebook }}</a>
            </div>
        </div>
    </div>

</div>