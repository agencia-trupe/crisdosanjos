@extends('frontend.common.template')

@section('content')

<div class="mensagens">
	<div class="center masonry-grid">
		@foreach($posts as $post)
		<div class="grid-item grid-mensagem">
			<a href="{{ $contato->instagram_link }}" class="mensagem" target="_blank">
				<img src="{{ $post->node->display_url }}" class="mensagem-capa" alt="">
				<div class="dados">
					<img src="{{ $user->graphql->user->profile_pic_url }}" alt="" class="img-perfil">
					@foreach($post->node->edge_media_to_caption->edges as $text)
					<p><strong>{{ $user->graphql->user->username }}</strong> {{ $text->node->text }}</p>
					@endforeach
				</div>
			</a>
		</div>
		@endforeach
	</div>
	<button class="btn-mensagens">ver mais +</button>

</div>

@endsection