export default function ConfirmarAgendamento() {
    $('body').on('click', '.btn-confirmar-agendamento', function(event) {
        event.preventDefault();

        var form    = $(this).closest('form'),
            _this   = this,
            message = 'Após essa confirmação, será enviado um e-mail informando ao cliente que seu horário está agendado.';

        bootbox.confirm({
            size: 'large',
            backdrop: true,
            title: '<strong>Deseja confirmar este agendamento?</strong>',
            message: message,
            buttons: {
                'cancel': {
                    label: 'Cancelar',
                    className: 'btn-default btn-sm'
                },
                'confirm': {
                    label: '<span class="glyphicon glyphicon-ok" style="margin-right:10px;"></span>Confirmar',
                    className: 'btn-success btn-sm'
                }
            },
            callback: function(result) {
                if (result) {
                    // if ($(_this).hasClass('btn-delete-link')) return window.location.href = $(_this)[0].href;
                    form.submit();
                }
            }
        });
    });
};
