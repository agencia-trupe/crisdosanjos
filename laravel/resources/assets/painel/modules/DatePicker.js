export default function DatePicker() {
  $(".datepicker").datepicker({
    closeText: "Fechar",
    prevText: "&#x3c;Anterior",
    nextText: "Pr&oacute;ximo&#x3e;",
    currentText: "Hoje",
    monthNames: [
      "Janeiro",
      "Fevereiro",
      "Mar&ccedil;o",
      "Abril",
      "Maio",
      "Junho",
      "Julho",
      "Agosto",
      "Setembro",
      "Outubro",
      "Novembro",
      "Dezembro",
    ],
    monthNamesShort: [
      "Jan",
      "Fev",
      "Mar",
      "Abr",
      "Mai",
      "Jun",
      "Jul",
      "Ago",
      "Set",
      "Out",
      "Nov",
      "Dez",
    ],
    dayNames: [
      "Domingo",
      "Segunda-feira",
      "Ter&ccedil;a-feira",
      "Quarta-feira",
      "Quinta-feira",
      "Sexta-feira",
      "S&aacute;bado",
    ],
    dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&aacute;b"],
    dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&aacute;b"],
    weekHeader: "Sm",
    dateFormat: "dd/mm/yy",
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: "",
    onSelect: function (dateText, inst) {
      $("#dataSelecionada").html("Data: " + dateText);
      $(".input-data").val(dateText);

      var url = window.location.pathname + "/" + dateText;

      $.ajax({
        type: "GET",
        url: url,
        success: function (data, textStatus, jqXHR) {
          console.log(data);
          data.forEach((element) => {
            var id = element[0].replace(":", "");
            $("#" + id).html(
              element[1] == 0 ? "Não liberado." : "Liberado para agendamento!"
            );
          });
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    },
  });

  if ($(".datepicker").val() == "")
    $(".datepicker").datepicker("setDate", new Date());

}
