import Clipboard from "./modules/Clipboard.js";
import DataTables from "./modules/DataTables.js";
import DatePicker from "./modules/DatePicker.js";
import DeleteButton from "./modules/DeleteButton.js";
import Filtro from "./modules/Filtro.js";
import GeneratorFields from "./modules/GeneratorFields.js";
import ImagesUpload from "./modules/ImagesUpload.js";
import MonthPicker from "./modules/MonthPicker.js";
import MultiSelect from "./modules/MultiSelect.js";
import OrderImages from "./modules/OrderImages.js";
import OrderTable from "./modules/OrderTable.js";
import TextEditor from "./modules/TextEditor.js";
import ConfirmarAgendamento from "./modules/ConfirmarAgendamento.js";

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});

Clipboard();
DataTables();
DatePicker();
DeleteButton();
Filtro();
GeneratorFields();
ImagesUpload();
MonthPicker();
MultiSelect();
OrderImages();
OrderTable();
TextEditor();
ConfirmarAgendamento();

$(document).ready(function () {
  var data = new Date(),
    dia = data.getDate().toString(),
    diaF = dia.length == 1 ? "0" + dia : dia,
    mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
    mesF = mes.length == 1 ? "0" + mes : mes,
    anoF = data.getFullYear();
  $("#dataSelecionada").html("Data: " + diaF + "/" + mesF + "/" + anoF);
  $(".input-data").val(diaF + "/" + mesF + "/" + anoF);

  function getUrlVars() {
    var hashes = window.location.href
      .slice(window.location.href.indexOf("?") + 1)
      .split("&");

    var hash = hashes[0].split("=");

    if (hash.length > 1) {
      var date = decodeURIComponent(hash[1]);
      console.log(date);
      $(".datepicker").datepicker("setDate", date);
      $("#dataSelecionada").html("Data: " + date);
      $(".input-data").val(date);
    }
  }
  getUrlVars();
  
});
