import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";
// import DatePicker from "../painel/modules/DatePicker";

AjaxSetup();
MobileToggle();
// DatePicker();

$(document).ready(function () {
  // DADOS AGENDAMENTO
  $(".btn-modal-agendamento").click(function () {
    $("#modalAgendamento").css("display", "block");

    var diaSemana = $(this).attr("semana");
    $(".dia-semana").html(diaSemana);

    var data = $(this).val();
    var dataCompleta = $(this).attr("completa");
    $(".dia-semana").html(diaSemana);
    $(".input-data").attr("value", dataCompleta);
    $(".dia-data").html(data);

    var horario = $(this).children(".horario").html();
    console.log(horario);
    $(".input-horario").attr("value", horario);
    $(".dia-horario").html(horario);
  });

  $("#btnAddAgendamento").click(function () {
    $("#modalAgendamento").css("display", "none");
    $("#modalAgendamentoMsg").css("display", "block");
  });

  $(".close").click(function () {
    $("#modalAgendamento").modal("hide");
    $("#modalAgendamentoMsg").modal("hide");
  });

  // CALENDÁRIOS POR DIA - P/ AGENDAMENTOS
  $(".tabelas-calendario").slick({
    dots: false,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    slidesPerRow: 1,
    infinite: false,
    mobileFirst: true,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1260,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          slidesPerRow: 1,
          centerMode: false,
        },
      },
    ],
  });

  // GRID ORAÇÕES E MENSAGENS - OK
  var $grid = $(".masonry-grid")
    .masonry({
      itemSelector: ".grid-item",
      columnWidth: ".grid-item",
      percentPosition: true,
    })
    .on("layoutComplete", function (event, laidOutItems) {
      $(".masonry-grid").masonry("layout");
    });

  // BTN VER MAIS MENSAGENS - OK
  var itensMensagens = $(".grid-mensagem");
  var spliceItensQuantidade = 6;

  if (itensMensagens.length <= spliceItensQuantidade) {
    $(".btn-mensagens").hide();
  }

  var setDivMensagem = function () {
    var spliceItens = itensMensagens.splice(0, spliceItensQuantidade);
    $(".masonry-grid").append(spliceItens);
    $(spliceItens).show();
    $(".masonry-grid").masonry("layout");
    if (itensMensagens.length <= 0) {
      $(".btn-mensagens").hide();
    }
  };

  $(".btn-mensagens").click(function () {
    setDivMensagem();
  });

  $(".grid-mensagem").hide();
  setDivMensagem();
});
